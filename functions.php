<?php
require 'lib/updater/plugin-update-checker.php';

/**
 * Child theme functions
 *
 * Functions file for child theme, enqueues parent and child stylesheets by default.
 *
 * @since	1.0.0
 * @package aa
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! function_exists( 'aa_enqueue_styles' ) ) {
	// Add enqueue function to the desired action.
	add_action( 'wp_enqueue_scripts', 'aa_enqueue_styles' );

	/**
	 * Enqueue Styles.
	 *
	 * Enqueue parent style and child styles where parent are the dependency
	 * for child styles so that parent styles always get enqueued first.
	 *
	 * @since 1.0.0
	 */
	function aa_enqueue_styles() {
		// Parent style variable.
		$parent_style = 'parent-style';

		// Enqueue Parent theme's stylesheet.
		wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );

		// Enqueue Child theme's stylesheet.
		// Setting 'parent-style' as a dependency will ensure that the child theme stylesheet loads after it.
		wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( $parent_style ) );
	}
}

if ( !is_admin() ) wp_deregister_script('jquery');

wp_deregister_script('jquery');
wp_deregister_script('minti-easing');
wp_deregister_script('waypoints');
wp_deregister_script('minti-waypoints-sticky');
wp_deregister_script('minti-prettyphoto');
wp_deregister_script('minti-isotope');
wp_deregister_script('minti-functions');
wp_deregister_script('minti-flexslider');

wp_dequeue_script('jquery');
wp_dequeue_script('minti-easing');
wp_dequeue_script('waypoints');
wp_dequeue_script('minti-waypoints-sticky');
wp_dequeue_script('minti-prettyphoto');
wp_dequeue_script('minti-isotope');
wp_dequeue_script('minti-functions');
wp_dequeue_script('minti-flexslider');

wp_dequeue_script('minti-smoothscroll');

wp_dequeue_script( 'comment-reply' );

// Remove other PrettyPhoto scripts
wp_dequeue_script( 'prettyPhoto' );
wp_deregister_script( 'prettyPhoto' );

$updateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitbucket.org/Gareth_/roboteam-child-theme/',
	__FILE__,
	'roboteam-child-theme'
);

$updateChecker->addFilter('first_check_time', function($unusedTimestamp) {
	//Always check for updates 1 hour after the first activation.
	return time() + 600;
});

$updateChecker->setBranch('release/latest');
